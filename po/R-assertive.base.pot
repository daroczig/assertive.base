msgid ""
msgstr ""
"Project-Id-Version: assertive.base\n"
"Report-Msgid-Bugs-To: https://bitbucket.org/richierocks/assertive.base\n"
"POT-Creation-Date: 2015-07-14 11:04\n"
"PO-Revision-Date: 2015-08-10 20:06\n"
"Last-Translator: Richard Cotton <richierocks@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"


msgid "The values of %s are not all FALSE."
msgstr ""

msgid "The values of %s are never FALSE."
msgstr ""

msgid "The values of %s are not all NA."
msgstr ""

msgid "The values of %s are never NA."
msgstr ""

msgid "The values of %s are not all TRUE."
msgstr ""

msgid "The values of %s are never TRUE."
msgstr ""

msgid "The values of %s are sometimes FALSE."
msgstr ""

msgid "The values of %s are all FALSE."
msgstr ""

msgid "The values of %s are sometimes NA."
msgstr ""

msgid "The values of %s are all NA."
msgstr ""

msgid "The values of %s are sometimes TRUE."
msgstr ""

msgid "The values of %s are all TRUE."
msgstr ""

msgid "The length of value should be 1 or the length of x (%d) but is %d."
msgstr ""

msgid "(showing the first %d)"
msgstr ""

msgid "You must provide a class."
msgstr ""

msgid "target_class should be a character vector."
msgstr ""

msgid "Coercing %s to class %s."
msgstr ""

msgid "%s cannot be coerced to type %s."
msgstr ""

msgid "%s cannot be coerced to any of these types: %s."
msgstr ""

msgid "%s is not identical to FALSE."
msgstr ""

msgid "%s is not identical to NA."
msgstr ""

msgid "%s is not identical to TRUE."
msgstr ""

msgid "Duplicated arguments: %s"
msgstr ""

msgid "%s has length 0."
msgstr ""

msgid "Only the first value of %s will be used."
msgstr ""

msgid        "There was %d failure%s:\n"
msgid_plural "There were %d failures%s:\n"
msgstr[0]    ""
msgstr[1]    ""
